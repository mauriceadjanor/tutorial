class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.integer :id_user
      t.integer :id_post
      t.text :body

      t.timestamps
    end
  end
end
